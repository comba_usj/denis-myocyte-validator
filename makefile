VALIDATOR_SRC = src/denis-myocyte-validator.cpp
BOINC_SRC = /home/boincadm/boinc
BOINC_SCHED = $(BOINC_SRC)/sched

MYSQL_LIB := $(shell mysql_config --libs)
MYSQL_INC := $(shell mysql_config --include)

all: validator validator_test

validator: $(BOINC_SCHED)/validator.cpp $(VALIDATOR_SRC) 
	g++ -O3 -g -I$(BOINC_SCHED) -I$(BOINC_SRC) -I$(BOINC_SRC)/lib -I$(BOINC_SRC)/db $(MYSQL_INC) -o bin/denis-myocyte-validator $(BOINC_SCHED)/validator.cpp $(VALIDATOR_SRC) $(BOINC_SCHED)/validate_util.cpp $(BOINC_SCHED)/validate_util2.cpp -L $(BOINC_SCHED) -lsched -L $(BOINC_SRC)/lib -lboinc $(MYSQL_LIB)

validator_test: $(BOINC_SCHED)/validator_test.cpp $(VALIDATOR_SRC)
	g++ -O3 -g -I$(BOINC_SCHED) -I$(BOINC_SRC) -I$(BOINC_SRC)/lib -I$(BOINC_SRC)/db $(MYSQL_INC) -o bin/denis-myocyte-validator_test $(BOINC_SCHED)/validator_test.cpp $(VALIDATOR_SRC) $(BOINC_SCHED)/validate_util.o -L $(BOINC_SCHED) -lsched -L $(BOINC_SRC)/lib -lboinc $(MYSQL_LIB)


test:
	cd ./tests; pwd; ./run_tests
	cd ./tests; ./clean_tests

clean:
	rm bin/*
