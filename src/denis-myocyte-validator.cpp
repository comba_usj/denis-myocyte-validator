// =========================================================================
//  --- DENIS@Home --
// 
//  - Program: Myocyte Validator
//
// -------------------------------------------------------------------------
//
// Validator for DENIS Myocyte Simulator
//
// -------------------------------------------------------------------------
//
// Developer:
//   Jesús Carro (jcarro@usj.es)
//
// DENIS@Home                                         Universidad San Jorge 
// http://denis.usj.es                                    http://www.usj.es
//
// =========================================================================
//  
// Copyright (C) 2018 Universidad San Jorge
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =========================================================================

#include "config.h"
#include "util.h"
#include "sched_util.h"
#include "sched_msgs.h"
#include "validate_util.h"
#include "validate_util2.h"
#include "validator.h"
#include "md5_file.h"
#include <cmath> 

using std::string;
using std::vector;
using std::abs;

int init_result(RESULT& result, void*& data) {

    int retval;
    vector<string> result_file_paths;
    FILE* result_file;
    vector<float>* result_numbers = new vector<float>;

    retval = get_output_file_paths(result, result_file_paths);
    if (retval) return retval;

    for(int i=0; i < result_file_paths.size(); i++){
        retval = try_fopen(result_file_paths[i].c_str(), result_file, "r");
        if (retval) return retval;

       	while(fgetc(result_file)!=(int)'\n' | feof(result_file));
	

        float number;
        fscanf(result_file, "%f", &number);

        while (!feof(result_file)){
            result_numbers->push_back(number);
            fscanf(result_file, "%f", &number);
        }

        fclose (result_file);
    }

    data = (void*)result_numbers;


    return 0;
}

int compare_results(
    RESULT & /*r1*/, void* data1,
    RESULT const& /*r2*/, void* data2,
    bool& match) {

    float *result_numbers_1 = ((vector<float> *)data1)->data();
    float *result_numbers_2 = ((vector<float> *)data2)->data();
    int length_1 = ((vector<float> *)data1)->size();
    int length_2 = ((vector<float> *)data2)->size();


    if(length_1 != length_2){
        match = false;
        return 0;
    }

    for (int i=0; i<length_1; i++){
         if(result_numbers_1[i]!=result_numbers_2[i]){
             if(abs(result_numbers_2[i])>1e-16){
                 if(abs(result_numbers_1[i]/result_numbers_2[i]-1)>0.001){
                     match=false;
                     return 0;
                 }
             } else {
		 if(abs(result_numbers_1[i])>1e-16){
		     match=false;
		     return 0;
		 }
	     }
         }
    }

    match = true;
    return 0;

}

int cleanup_result(RESULT const& /*result*/, void* data) {

   delete (vector<float>*)data;

    return 0;
}

void validate_handler_usage(){
}

int validate_handler_init(int argc, char** argv){

   return 0;
}
